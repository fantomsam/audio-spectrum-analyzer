EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title "Audio Spectrum Analyser"
Date "2020-09-09"
Rev "1.0"
Comp ""
Comment1 "License: TAPR Open Hardware v1.0"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7200 3850 7200 3750
Connection ~ 7200 3050
Wire Wire Line
	7200 3050 7200 2950
Connection ~ 7200 3150
Wire Wire Line
	7200 3150 7200 3050
Connection ~ 7200 3250
Wire Wire Line
	7200 3250 7200 3150
Connection ~ 7200 3350
Wire Wire Line
	7200 3350 7200 3250
Connection ~ 7200 3450
Wire Wire Line
	7200 3450 7200 3350
Connection ~ 7200 3550
Wire Wire Line
	7200 3550 7200 3450
Connection ~ 7200 3650
Wire Wire Line
	7200 3650 7200 3550
Connection ~ 7200 3750
Wire Wire Line
	7200 3750 7200 3650
Wire Wire Line
	6500 2950 6500 3050
Connection ~ 6500 3050
Wire Wire Line
	6500 3050 6500 3150
Connection ~ 6500 3150
Wire Wire Line
	6500 3150 6500 3250
Connection ~ 6500 3250
Wire Wire Line
	6500 3250 6500 3350
Connection ~ 6500 3350
Wire Wire Line
	6500 3350 6500 3450
Connection ~ 6500 3450
Wire Wire Line
	6500 3450 6500 3550
Connection ~ 6500 3550
Wire Wire Line
	6500 3550 6500 3650
Connection ~ 6500 3650
Wire Wire Line
	6500 3650 6500 3750
Connection ~ 6500 3750
Wire Wire Line
	6500 3750 6500 3850
Wire Wire Line
	5800 3850 5800 3750
Connection ~ 5800 3050
Wire Wire Line
	5800 3050 5800 2950
Connection ~ 5800 3150
Wire Wire Line
	5800 3150 5800 3050
Connection ~ 5800 3250
Wire Wire Line
	5800 3250 5800 3150
Connection ~ 5800 3350
Wire Wire Line
	5800 3350 5800 3250
Connection ~ 5800 3450
Wire Wire Line
	5800 3450 5800 3350
Connection ~ 5800 3550
Wire Wire Line
	5800 3550 5800 3450
Connection ~ 5800 3650
Wire Wire Line
	5800 3650 5800 3550
Connection ~ 5800 3750
Wire Wire Line
	5800 3750 5800 3650
Wire Wire Line
	5100 3850 5100 3750
Connection ~ 5100 3050
Wire Wire Line
	5100 3050 5100 2950
Connection ~ 5100 3150
Wire Wire Line
	5100 3150 5100 3050
Connection ~ 5100 3250
Wire Wire Line
	5100 3250 5100 3150
Connection ~ 5100 3350
Wire Wire Line
	5100 3350 5100 3250
Connection ~ 5100 3450
Wire Wire Line
	5100 3450 5100 3350
Connection ~ 5100 3550
Wire Wire Line
	5100 3550 5100 3450
Connection ~ 5100 3650
Wire Wire Line
	5100 3650 5100 3550
Connection ~ 5100 3750
Wire Wire Line
	5100 3750 5100 3650
Wire Wire Line
	4400 2950 4400 3050
Connection ~ 4400 3050
Wire Wire Line
	4400 3050 4400 3150
Connection ~ 4400 3150
Wire Wire Line
	4400 3150 4400 3250
Connection ~ 4400 3250
Wire Wire Line
	4400 3250 4400 3350
Connection ~ 4400 3350
Wire Wire Line
	4400 3350 4400 3450
Connection ~ 4400 3450
Wire Wire Line
	4400 3450 4400 3550
Connection ~ 4400 3550
Wire Wire Line
	4400 3550 4400 3650
Connection ~ 4400 3650
Wire Wire Line
	4400 3650 4400 3750
Connection ~ 4400 3750
Wire Wire Line
	4400 3750 4400 3850
Wire Wire Line
	3700 2950 3700 3050
Connection ~ 3700 3050
Wire Wire Line
	3700 3050 3700 3150
Connection ~ 3700 3150
Wire Wire Line
	3700 3150 3700 3250
Connection ~ 3700 3250
Wire Wire Line
	3700 3250 3700 3350
Connection ~ 3700 3350
Wire Wire Line
	3700 3350 3700 3450
Connection ~ 3700 3450
Wire Wire Line
	3700 3450 3700 3550
Connection ~ 3700 3550
Wire Wire Line
	3700 3550 3700 3650
Connection ~ 3700 3650
Wire Wire Line
	3700 3650 3700 3750
Connection ~ 3700 3750
Wire Wire Line
	3700 3750 3700 3850
Wire Wire Line
	3000 2950 3000 3050
Connection ~ 3000 3050
Wire Wire Line
	3000 3050 3000 3150
Connection ~ 3000 3150
Wire Wire Line
	3000 3150 3000 3250
Connection ~ 3000 3250
Wire Wire Line
	3000 3250 3000 3350
Connection ~ 3000 3350
Wire Wire Line
	3000 3350 3000 3450
Connection ~ 3000 3450
Wire Wire Line
	3000 3450 3000 3550
Connection ~ 3000 3550
Wire Wire Line
	3000 3550 3000 3650
Connection ~ 3000 3650
Wire Wire Line
	3000 3650 3000 3750
Connection ~ 3000 3750
Wire Wire Line
	3000 3750 3000 3850
Entry Wire Line
	7750 2950 7850 3050
Entry Wire Line
	7750 3050 7850 3150
Entry Wire Line
	7750 3150 7850 3250
Entry Wire Line
	7750 3250 7850 3350
Entry Wire Line
	7750 3350 7850 3450
Entry Wire Line
	7750 3450 7850 3550
Entry Wire Line
	7750 3550 7850 3650
Entry Wire Line
	7750 3650 7850 3750
Entry Wire Line
	7750 3750 7850 3850
Entry Wire Line
	7750 3850 7850 3950
Text Label 7600 2950 0    50   ~ 0
LED0
Wire Wire Line
	7600 2950 7750 2950
Wire Wire Line
	7600 3050 7750 3050
Wire Wire Line
	7600 3150 7750 3150
Wire Wire Line
	7600 3250 7750 3250
Wire Wire Line
	7600 3350 7750 3350
Wire Wire Line
	7600 3450 7750 3450
Wire Wire Line
	7600 3550 7750 3550
Wire Wire Line
	7600 3650 7750 3650
Wire Wire Line
	7600 3750 7750 3750
Wire Wire Line
	7600 3850 7750 3850
Text Label 7600 3050 0    50   ~ 0
LED1
Text Label 7600 3150 0    50   ~ 0
LED2
Text Label 7600 3250 0    50   ~ 0
LED3
Text Label 7600 3350 0    50   ~ 0
LED4
Text Label 7600 3450 0    50   ~ 0
LED5
Text Label 7600 3550 0    50   ~ 0
LED6
Text Label 7600 3650 0    50   ~ 0
LED7
Text Label 7600 3750 0    50   ~ 0
LED8
Text Label 7600 3850 0    50   ~ 0
LED9
Entry Wire Line
	7050 2950 7150 3050
Entry Wire Line
	7050 3050 7150 3150
Entry Wire Line
	7050 3150 7150 3250
Entry Wire Line
	7050 3250 7150 3350
Entry Wire Line
	7050 3350 7150 3450
Entry Wire Line
	7050 3450 7150 3550
Entry Wire Line
	7050 3550 7150 3650
Entry Wire Line
	7050 3650 7150 3750
Entry Wire Line
	7050 3750 7150 3850
Entry Wire Line
	7050 3850 7150 3950
Text Label 6900 2950 0    50   ~ 0
LED0
Wire Wire Line
	6900 2950 7050 2950
Wire Wire Line
	6900 3050 7050 3050
Wire Wire Line
	6900 3150 7050 3150
Wire Wire Line
	6900 3250 7050 3250
Wire Wire Line
	6900 3350 7050 3350
Wire Wire Line
	6900 3450 7050 3450
Wire Wire Line
	6900 3550 7050 3550
Wire Wire Line
	6900 3650 7050 3650
Wire Wire Line
	6900 3750 7050 3750
Wire Wire Line
	6900 3850 7050 3850
Text Label 6900 3050 0    50   ~ 0
LED1
Text Label 6900 3150 0    50   ~ 0
LED2
Text Label 6900 3250 0    50   ~ 0
LED3
Text Label 6900 3350 0    50   ~ 0
LED4
Text Label 6900 3450 0    50   ~ 0
LED5
Text Label 6900 3550 0    50   ~ 0
LED6
Text Label 6900 3650 0    50   ~ 0
LED7
Text Label 6900 3750 0    50   ~ 0
LED8
Text Label 6900 3850 0    50   ~ 0
LED9
Entry Wire Line
	6350 2950 6450 3050
Entry Wire Line
	6350 3050 6450 3150
Entry Wire Line
	6350 3150 6450 3250
Entry Wire Line
	6350 3250 6450 3350
Entry Wire Line
	6350 3350 6450 3450
Entry Wire Line
	6350 3450 6450 3550
Entry Wire Line
	6350 3550 6450 3650
Entry Wire Line
	6350 3650 6450 3750
Entry Wire Line
	6350 3750 6450 3850
Entry Wire Line
	6350 3850 6450 3950
Text Label 6200 2950 0    50   ~ 0
LED0
Wire Wire Line
	6200 2950 6350 2950
Wire Wire Line
	6200 3050 6350 3050
Wire Wire Line
	6200 3150 6350 3150
Wire Wire Line
	6200 3250 6350 3250
Wire Wire Line
	6200 3350 6350 3350
Wire Wire Line
	6200 3450 6350 3450
Wire Wire Line
	6200 3550 6350 3550
Wire Wire Line
	6200 3650 6350 3650
Wire Wire Line
	6200 3750 6350 3750
Wire Wire Line
	6200 3850 6350 3850
Text Label 6200 3050 0    50   ~ 0
LED1
Text Label 6200 3150 0    50   ~ 0
LED2
Text Label 6200 3250 0    50   ~ 0
LED3
Text Label 6200 3350 0    50   ~ 0
LED4
Text Label 6200 3450 0    50   ~ 0
LED5
Text Label 6200 3550 0    50   ~ 0
LED6
Text Label 6200 3650 0    50   ~ 0
LED7
Text Label 6200 3750 0    50   ~ 0
LED8
Text Label 6200 3850 0    50   ~ 0
LED9
Entry Wire Line
	5650 2950 5750 3050
Entry Wire Line
	5650 3050 5750 3150
Entry Wire Line
	5650 3150 5750 3250
Entry Wire Line
	5650 3250 5750 3350
Entry Wire Line
	5650 3350 5750 3450
Entry Wire Line
	5650 3450 5750 3550
Entry Wire Line
	5650 3550 5750 3650
Entry Wire Line
	5650 3650 5750 3750
Entry Wire Line
	5650 3750 5750 3850
Entry Wire Line
	5650 3850 5750 3950
Text Label 5500 2950 0    50   ~ 0
LED0
Wire Wire Line
	5500 2950 5650 2950
Wire Wire Line
	5500 3050 5650 3050
Wire Wire Line
	5500 3150 5650 3150
Wire Wire Line
	5500 3250 5650 3250
Wire Wire Line
	5500 3350 5650 3350
Wire Wire Line
	5500 3450 5650 3450
Wire Wire Line
	5500 3550 5650 3550
Wire Wire Line
	5500 3650 5650 3650
Wire Wire Line
	5500 3750 5650 3750
Wire Wire Line
	5500 3850 5650 3850
Text Label 5500 3050 0    50   ~ 0
LED1
Text Label 5500 3150 0    50   ~ 0
LED2
Text Label 5500 3250 0    50   ~ 0
LED3
Text Label 5500 3350 0    50   ~ 0
LED4
Text Label 5500 3450 0    50   ~ 0
LED5
Text Label 5500 3550 0    50   ~ 0
LED6
Text Label 5500 3650 0    50   ~ 0
LED7
Text Label 5500 3750 0    50   ~ 0
LED8
Text Label 5500 3850 0    50   ~ 0
LED9
Entry Wire Line
	4950 2950 5050 3050
Entry Wire Line
	4950 3050 5050 3150
Entry Wire Line
	4950 3150 5050 3250
Entry Wire Line
	4950 3250 5050 3350
Entry Wire Line
	4950 3350 5050 3450
Entry Wire Line
	4950 3450 5050 3550
Entry Wire Line
	4950 3550 5050 3650
Entry Wire Line
	4950 3650 5050 3750
Entry Wire Line
	4950 3750 5050 3850
Entry Wire Line
	4950 3850 5050 3950
Text Label 4800 2950 0    50   ~ 0
LED0
Wire Wire Line
	4800 2950 4950 2950
Wire Wire Line
	4800 3050 4950 3050
Wire Wire Line
	4800 3150 4950 3150
Wire Wire Line
	4800 3250 4950 3250
Wire Wire Line
	4800 3350 4950 3350
Wire Wire Line
	4800 3450 4950 3450
Wire Wire Line
	4800 3550 4950 3550
Wire Wire Line
	4800 3650 4950 3650
Wire Wire Line
	4800 3750 4950 3750
Wire Wire Line
	4800 3850 4950 3850
Text Label 4800 3050 0    50   ~ 0
LED1
Text Label 4800 3150 0    50   ~ 0
LED2
Text Label 4800 3250 0    50   ~ 0
LED3
Text Label 4800 3350 0    50   ~ 0
LED4
Text Label 4800 3450 0    50   ~ 0
LED5
Text Label 4800 3550 0    50   ~ 0
LED6
Text Label 4800 3650 0    50   ~ 0
LED7
Text Label 4800 3750 0    50   ~ 0
LED8
Text Label 4800 3850 0    50   ~ 0
LED9
Entry Wire Line
	4250 2950 4350 3050
Entry Wire Line
	4250 3050 4350 3150
Entry Wire Line
	4250 3150 4350 3250
Entry Wire Line
	4250 3250 4350 3350
Entry Wire Line
	4250 3350 4350 3450
Entry Wire Line
	4250 3450 4350 3550
Entry Wire Line
	4250 3550 4350 3650
Entry Wire Line
	4250 3650 4350 3750
Entry Wire Line
	4250 3750 4350 3850
Entry Wire Line
	4250 3850 4350 3950
Text Label 4100 2950 0    50   ~ 0
LED0
Wire Wire Line
	4100 2950 4250 2950
Wire Wire Line
	4100 3050 4250 3050
Wire Wire Line
	4100 3150 4250 3150
Wire Wire Line
	4100 3250 4250 3250
Wire Wire Line
	4100 3350 4250 3350
Wire Wire Line
	4100 3450 4250 3450
Wire Wire Line
	4100 3550 4250 3550
Wire Wire Line
	4100 3650 4250 3650
Wire Wire Line
	4100 3750 4250 3750
Wire Wire Line
	4100 3850 4250 3850
Text Label 4100 3050 0    50   ~ 0
LED1
Text Label 4100 3150 0    50   ~ 0
LED2
Text Label 4100 3250 0    50   ~ 0
LED3
Text Label 4100 3350 0    50   ~ 0
LED4
Text Label 4100 3450 0    50   ~ 0
LED5
Text Label 4100 3550 0    50   ~ 0
LED6
Text Label 4100 3650 0    50   ~ 0
LED7
Text Label 4100 3750 0    50   ~ 0
LED8
Text Label 4100 3850 0    50   ~ 0
LED9
Entry Wire Line
	3550 2950 3650 3050
Entry Wire Line
	3550 3050 3650 3150
Entry Wire Line
	3550 3150 3650 3250
Entry Wire Line
	3550 3250 3650 3350
Entry Wire Line
	3550 3350 3650 3450
Entry Wire Line
	3550 3450 3650 3550
Entry Wire Line
	3550 3550 3650 3650
Entry Wire Line
	3550 3650 3650 3750
Entry Wire Line
	3550 3750 3650 3850
Entry Wire Line
	3550 3850 3650 3950
Text Label 3400 2950 0    50   ~ 0
LED0
Wire Wire Line
	3400 2950 3550 2950
Wire Wire Line
	3400 3050 3550 3050
Wire Wire Line
	3400 3150 3550 3150
Wire Wire Line
	3400 3250 3550 3250
Wire Wire Line
	3400 3350 3550 3350
Wire Wire Line
	3400 3450 3550 3450
Wire Wire Line
	3400 3550 3550 3550
Wire Wire Line
	3400 3650 3550 3650
Wire Wire Line
	3400 3750 3550 3750
Wire Wire Line
	3400 3850 3550 3850
Text Label 3400 3050 0    50   ~ 0
LED1
Text Label 3400 3150 0    50   ~ 0
LED2
Text Label 3400 3250 0    50   ~ 0
LED3
Text Label 3400 3350 0    50   ~ 0
LED4
Text Label 3400 3450 0    50   ~ 0
LED5
Text Label 3400 3550 0    50   ~ 0
LED6
Text Label 3400 3650 0    50   ~ 0
LED7
Text Label 3400 3750 0    50   ~ 0
LED8
Text Label 3400 3850 0    50   ~ 0
LED9
Wire Bus Line
	7850 4100 7150 4100
Wire Bus Line
	7150 4100 6450 4100
Connection ~ 7150 4100
Wire Bus Line
	6450 4100 5750 4100
Connection ~ 6450 4100
Wire Bus Line
	3650 4100 4350 4100
Connection ~ 5750 4100
Connection ~ 5050 4100
Wire Bus Line
	5050 4100 5750 4100
Connection ~ 4350 4100
Wire Bus Line
	4350 4100 5050 4100
$Comp
L Device:Q_PMOS_GSD Q1
U 1 1 5F6AB653
P 3100 2550
F 0 "Q1" H 2950 2500 50  0000 L CNN
F 1 "Si2305DS" V 3350 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 3300 2650 50  0001 C CNN
F 3 "~" H 3100 2550 50  0001 C CNN
	1    3100 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q2
U 1 1 5F6B29A4
P 3800 2550
F 0 "Q2" H 3650 2500 50  0000 L CNN
F 1 "Si2305DS" V 4050 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4000 2650 50  0001 C CNN
F 3 "~" H 3800 2550 50  0001 C CNN
	1    3800 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q3
U 1 1 5F6B3559
P 4500 2550
F 0 "Q3" H 4350 2500 50  0000 L CNN
F 1 "Si2305DS" V 4750 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 4700 2650 50  0001 C CNN
F 3 "~" H 4500 2550 50  0001 C CNN
	1    4500 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q4
U 1 1 5F6B3D7E
P 5200 2550
F 0 "Q4" H 5050 2500 50  0000 L CNN
F 1 "Si2305DS" V 5450 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 5400 2650 50  0001 C CNN
F 3 "~" H 5200 2550 50  0001 C CNN
	1    5200 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q5
U 1 1 5F6B7556
P 5900 2550
F 0 "Q5" H 5750 2500 50  0000 L CNN
F 1 "Si2305DS" V 6150 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 6100 2650 50  0001 C CNN
F 3 "~" H 5900 2550 50  0001 C CNN
	1    5900 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q6
U 1 1 5F6B755C
P 6600 2550
F 0 "Q6" H 6450 2500 50  0000 L CNN
F 1 "Si2305DS" V 6850 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 6800 2650 50  0001 C CNN
F 3 "~" H 6600 2550 50  0001 C CNN
	1    6600 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	3000 2750 3000 2950
Connection ~ 3000 2950
Wire Wire Line
	3700 2750 3700 2950
Connection ~ 3700 2950
Wire Wire Line
	4400 2750 4400 2950
Connection ~ 4400 2950
Wire Wire Line
	5100 2750 5100 2950
Connection ~ 5100 2950
Wire Wire Line
	5800 2750 5800 2950
Connection ~ 5800 2950
Wire Wire Line
	6500 2750 6500 2950
Connection ~ 6500 2950
Wire Wire Line
	7200 2750 7200 2950
Connection ~ 7200 2950
Wire Wire Line
	7200 2350 7200 2300
Wire Wire Line
	6500 2300 6500 2350
Wire Wire Line
	3000 2350 3000 2300
Wire Wire Line
	3000 2300 3700 2300
Connection ~ 6500 2300
Wire Wire Line
	6500 2300 7200 2300
Wire Wire Line
	5800 2350 5800 2300
Connection ~ 5800 2300
Wire Wire Line
	5800 2300 6500 2300
Wire Wire Line
	5100 2350 5100 2300
Connection ~ 5100 2300
Wire Wire Line
	5100 2300 5800 2300
Wire Wire Line
	3700 2350 3700 2300
Connection ~ 3700 2300
Wire Wire Line
	3700 2300 4400 2300
Wire Wire Line
	4400 2350 4400 2300
Connection ~ 4400 2300
Wire Wire Line
	4400 2300 5100 2300
Text GLabel 3300 2550 2    50   Input ~ 0
BAR1
Text GLabel 4000 2550 2    50   Input ~ 0
BAR2
Text GLabel 4700 2550 2    50   Input ~ 0
BAR3
Text GLabel 5400 2550 2    50   Input ~ 0
BAR4
Text GLabel 6100 2550 2    50   Input ~ 0
BAR5
Text GLabel 6800 2550 2    50   Input ~ 0
BAR6
Text GLabel 7500 2550 2    50   Input ~ 0
BAR7
$Comp
L power:VSS #PWR051
U 1 1 5F7B588C
P 5100 2250
F 0 "#PWR051" H 5100 2100 50  0001 C CNN
F 1 "VSS" H 5115 2423 50  0000 C CNN
F 2 "" H 5100 2250 50  0001 C CNN
F 3 "" H 5100 2250 50  0001 C CNN
	1    5100 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 2250 5100 2300
$Comp
L Connector_Generic:Conn_02x07_Odd_Even J?
U 1 1 5F692DB6
P 8650 3350
AR Path="/5F5665FD/5F692DB6" Ref="J?"  Part="1" 
AR Path="/5F566F48/5F692DB6" Ref="J6"  Part="1" 
F 0 "J6" H 8650 3750 50  0000 C CNN
F 1 "Conn_02x07_Top_Bottom" H 9050 2900 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x07_P2.54mm_Vertical" H 8650 3350 50  0001 C CNN
F 3 "~" H 8650 3350 50  0001 C CNN
	1    8650 3350
	1    0    0    -1  
$EndComp
Text GLabel 8450 3050 0    50   Input ~ 0
BAR1
Text GLabel 8450 3150 0    50   Input ~ 0
BAR2
Text GLabel 8450 3250 0    50   Input ~ 0
BAR3
Text GLabel 8450 3350 0    50   Input ~ 0
BAR4
Text GLabel 8450 3450 0    50   Input ~ 0
BAR5
Text GLabel 8450 3550 0    50   Input ~ 0
BAR6
Text GLabel 8450 3650 0    50   Input ~ 0
BAR7
Wire Wire Line
	8950 3050 9000 3050
Wire Wire Line
	9000 3050 9000 3150
Wire Wire Line
	9000 3650 8950 3650
Wire Wire Line
	8950 3550 9000 3550
Connection ~ 9000 3550
Wire Wire Line
	9000 3550 9000 3650
Wire Wire Line
	8950 3450 9000 3450
Connection ~ 9000 3450
Wire Wire Line
	9000 3450 9000 3550
Wire Wire Line
	8950 3350 9000 3350
Connection ~ 9000 3350
Wire Wire Line
	9000 3350 9000 3450
Wire Wire Line
	8950 3250 9000 3250
Connection ~ 9000 3250
Wire Wire Line
	9000 3250 9000 3350
Wire Wire Line
	8950 3150 9000 3150
Connection ~ 9000 3150
Wire Wire Line
	9000 3150 9000 3250
Wire Wire Line
	9000 3350 9100 3350
Wire Wire Line
	9100 3350 9100 3250
$Comp
L power:VSS #PWR052
U 1 1 5F7B551F
P 9100 3250
F 0 "#PWR052" H 9100 3100 50  0001 C CNN
F 1 "VSS" H 9115 3423 50  0000 C CNN
F 2 "" H 9100 3250 50  0001 C CNN
F 3 "" H 9100 3250 50  0001 C CNN
	1    9100 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_PMOS_GSD Q7
U 1 1 5F6B7562
P 7300 2550
F 0 "Q7" H 7150 2500 50  0000 L CNN
F 1 "Si2305DS" V 7550 2400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23_Handsoldering" H 7500 2650 50  0001 C CNN
F 3 "~" H 7300 2550 50  0001 C CNN
	1    7300 2550
	-1   0    0    1   
$EndComp
Entry Wire Line
	7850 4200 7950 4300
Connection ~ 7850 4100
Wire Wire Line
	7950 4300 8200 4300
Entry Wire Line
	7850 4300 7950 4400
Entry Wire Line
	7850 4400 7950 4500
Entry Wire Line
	7850 4500 7950 4600
Entry Wire Line
	7850 4600 7950 4700
Entry Wire Line
	7850 4700 7950 4800
Entry Wire Line
	7850 4800 7950 4900
Entry Wire Line
	7850 4900 7950 5000
Entry Wire Line
	7850 5000 7950 5100
Entry Wire Line
	7850 5100 7950 5200
Wire Wire Line
	8200 5200 7950 5200
Wire Wire Line
	7950 5100 8200 5100
Wire Wire Line
	7950 5000 8200 5000
Wire Wire Line
	7950 4400 8200 4400
Wire Wire Line
	7950 4500 8200 4500
Wire Wire Line
	7950 4600 8200 4600
Wire Wire Line
	7950 4700 8200 4700
Wire Wire Line
	8200 4800 7950 4800
Wire Wire Line
	7950 4900 8200 4900
Text Label 8000 4300 0    50   ~ 0
LED0
Text Label 8000 4400 0    50   ~ 0
LED1
Text Label 8000 4500 0    50   ~ 0
LED2
Text Label 8000 4600 0    50   ~ 0
LED3
Text Label 8000 4700 0    50   ~ 0
LED4
Text Label 8000 4800 0    50   ~ 0
LED5
Text Label 8000 4900 0    50   ~ 0
LED6
Text Label 8000 5000 0    50   ~ 0
LED7
Text Label 8000 5100 0    50   ~ 0
LED8
Text Label 8000 5200 0    50   ~ 0
LED9
$Comp
L Graphic:Logo_Open_Hardware_Small Open_Source_hardware?
U 1 1 5F87E8F3
P 10750 6850
AR Path="/5F5665FD/5F87E8F3" Ref="Open_Source_hardware?"  Part="1" 
AR Path="/5F566F48/5F87E8F3" Ref="Open_Source_hardware1"  Part="1" 
F 0 "Open_Source_hardware1" H 10750 7125 50  0001 C CNN
F 1 "Open Source Hardware" H 10750 6625 50  0000 C CNN
F 2 "Symbol:OSHW-Symbol_6.7x6mm_SilkScreen" H 10750 6850 50  0001 C CNN
F 3 "~" H 10750 6850 50  0001 C CNN
	1    10750 6850
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR7
U 1 1 5F5B9F10
P 7400 3350
F 0 "BAR7" H 7400 3925 50  0000 C CNN
F 1 "16kHz" H 7400 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 7400 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 5400 3550 50  0001 C CNN
	1    7400 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR6
U 1 1 5F5B9F0A
P 6700 3350
F 0 "BAR6" H 6700 3925 50  0000 C CNN
F 1 "6.25kHz" H 6700 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 6700 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 4700 3550 50  0001 C CNN
	1    6700 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR5
U 1 1 5F5B9F04
P 6000 3350
F 0 "BAR5" H 6000 3925 50  0000 C CNN
F 1 "2.5kHz" H 6000 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 6000 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 4000 3550 50  0001 C CNN
	1    6000 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR4
U 1 1 5F5AD902
P 5300 3350
F 0 "BAR4" H 5300 3925 50  0000 C CNN
F 1 "1kHz" H 5300 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 5300 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 3300 3550 50  0001 C CNN
	1    5300 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR3
U 1 1 5F5AD8FC
P 4600 3350
F 0 "BAR3" H 4600 3925 50  0000 C CNN
F 1 "400Hz" H 4600 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 4600 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 2600 3550 50  0001 C CNN
	1    4600 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR2
U 1 1 5F5A3666
P 3900 3350
F 0 "BAR2" H 3900 3925 50  0000 C CNN
F 1 "160Hz" H 3900 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 3900 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 1900 3550 50  0001 C CNN
	1    3900 3350
	1    0    0    -1  
$EndComp
$Comp
L LED:HDSP-4832_2 BAR1
U 1 1 5F59EFB0
P 3200 3350
F 0 "BAR1" H 3200 3925 50  0000 C CNN
F 1 "63Hz" H 3200 2700 50  0000 C CNN
F 2 "Local_footprints:display_10leds" H 3200 2550 50  0001 C CNN
F 3 "https://docs.broadcom.com/docs/AV02-1798EN" H 1200 3550 50  0001 C CNN
	1    3200 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x10_Female J?
U 1 1 5F7D6DD3
P 8400 4700
AR Path="/5F5665FD/5F7D6DD3" Ref="J?"  Part="1" 
AR Path="/5F566F48/5F7D6DD3" Ref="J7"  Part="1" 
F 0 "J7" H 8350 4100 50  0000 L CNN
F 1 "Conn_01x10" H 8480 4601 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x10_P2.54mm_Vertical" H 8400 4700 50  0001 C CNN
F 3 "~" H 8400 4700 50  0001 C CNN
	1    8400 4700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R11
U 1 1 5F78BD6C
P 8050 2550
F 0 "R11" H 8118 2596 50  0000 L CNN
F 1 "100" H 8118 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.30x1.75mm_HandSolder" V 8090 2540 50  0001 C CNN
F 3 "~" H 8050 2550 50  0001 C CNN
	1    8050 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7200 2300 8050 2300
Wire Wire Line
	8050 2300 8050 2400
Connection ~ 7200 2300
Text GLabel 8050 2800 3    50   Input ~ 0
LED0
Wire Wire Line
	8050 2800 8050 2700
Wire Bus Line
	7850 4100 7850 5100
Wire Bus Line
	3650 3050 3650 4100
Wire Bus Line
	4350 3050 4350 4100
Wire Bus Line
	5050 3050 5050 4100
Wire Bus Line
	5750 3050 5750 4100
Wire Bus Line
	6450 3050 6450 4100
Wire Bus Line
	7150 3050 7150 4100
Wire Bus Line
	7850 3050 7850 4100
$EndSCHEMATC

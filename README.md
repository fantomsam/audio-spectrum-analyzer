<div align="center"> <img src="./art/MSGEQ7_eval.png" width="500"> </div>

# Audio Spectrum Analyzer
This repository related to a project that have as goal the design of one bargraph type spectrum analyzer for the audio range (60-16kHz).

## Design
The Design of the analyzer was based on the `MSGEQ7` where is an integrated array of switch cap filters, peak detectors and an analog multiplexer.

For more information about this IC take a look at the [datasheet](https://www.mix-sig.com/images/datasheets/MSGEQ7.pdf).
